pimcore.registerNS("pimcore.plugin.kitt3nPimcoreContentElementsBundle");

pimcore.plugin.kitt3nPimcoreContentElementsBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.kitt3nPimcoreContentElementsBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("kitt3nPimcoreContentElementsBundle ready!");
    }
});

var kitt3nPimcoreContentElementsBundlePlugin = new pimcore.plugin.kitt3nPimcoreContentElementsBundle();
