<?php

namespace KITT3N\Pimcore\ContentElementsBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends FrontendController
{
    /**
     * @Route("/kitt3n_pimcore_content_elements")
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello world from kitt3n_pimcore_content_elements');
    }
}
