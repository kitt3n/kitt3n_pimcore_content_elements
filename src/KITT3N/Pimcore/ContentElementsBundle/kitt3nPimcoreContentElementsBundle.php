<?php

namespace KITT3N\Pimcore\ContentElementsBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class kitt3nPimcoreContentElementsBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/kitt3npimcorecontentelements/js/pimcore/startup.js'
        ];
    }
}